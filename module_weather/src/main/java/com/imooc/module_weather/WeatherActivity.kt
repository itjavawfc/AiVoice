package com.imooc.module_weather
import android.os.Bundle
import android.util.Log
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.utils.L

class WeatherActivity : BaseActivity() {

    private   val TAG = "WeatherActivity"
    override fun getLayoutId(): Int {
        return R.layout.activity_weather
    }

    override fun getTitleText(): String {
       return getString(R.string.app_name)
    }

    override fun initView() {
       L.d(" initView")
    }

    override fun isShowBack(): Boolean {
        return true
    }


}