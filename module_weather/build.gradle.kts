plugins {
   // id("com.android.application")
    if (ModuleConfig.isApp) {
        id("com.android.application")
    } else {
        id("com.android.library")
    }
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
}

android {
    namespace = "com.imooc.module_weather"
    compileSdk = 34

    defaultConfig {
     //   applicationId = "com.imooc.module_weather"
        minSdk = 24
        targetSdk = 33
      //  versionCode = 1
     //   versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        kapt {
            arguments {
                // arguments = [AROUTER_MODULE_NAME: project.getName()]
                // arguments = mapOf("AROUTER_MODULE_NAME" to project.name)
                arg("AROUTER_MODULE_NAME" , project.name)
            }
        }

    /*    kapt {
            arguments {
                // 路由框架译配置
                // module名称
                arg("AROUTER_MODULE_NAME", project.getName())
                // 是否生成路由文档，"enable"：生成文档，其余字符串不生成路由文档
                arg("AROUTER_GENERATE_DOC", "enable")
            }
        }*/
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.activity:activity-compose:1.8.1")
    implementation(platform("androidx.compose:compose-bom:2023.10.01"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3:1.1.2")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.10.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    api(project(":lib_base"))
    kapt("com.alibaba:arouter-compiler:1.5.2")
}