package com.imooc.constellation
import android.os.Bundle
import android.util.Log
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.utils.L

class ConstellationActivity : BaseActivity() {
    private   val TAG = "ConstellationActivity"
    override fun getLayoutId(): Int {
        return  R.layout.activity_constellation
    }

    override fun getTitleText(): String {
        return getString(R.string.app_name)
    }

    override fun initView() {
        L.d(" initView")
    }

    override fun isShowBack(): Boolean {
         return  true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     }
}