package com.imooc.map.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.utils.L
import com.imooc.map.R

class MainActivity : BaseActivity() {
    private   val TAG = "MainActivity"
    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getTitleText(): String {
       return getString(R.string.app_name)
    }

    override fun initView() {
        L.d("initView ")
    }

    override fun isShowBack(): Boolean {
        return  true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}