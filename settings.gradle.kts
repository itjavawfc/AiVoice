pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
     /*   maven {
            url : "http://maven.aliyun.com/nexus/content/groups/public/"
            allowInsecureProtocol = true

        }
        maven { setUrl("http://maven.aliyun.com/nexus/content/repositories/jcenter") }
        maven { setUrl("http://maven.aliyun.com/nexus/content/repositories/google") }
        maven { setUrl("http://maven.aliyun.com/nexus/content/repositories/gradle-plugin") }
*/
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "AiVoice"
include(":app")


include(":lib_base")
include(":lib_work")
include(":lib_voice")
include(":module_app_manager")
include(":module_constellation")
include(":module_developer")
include(":module_joke")
include(":module_map")
include(":module_setting")
include(":module_voice_setting")
include(":module_weather")
rootProject.buildFileName="build.gradle.kts"
