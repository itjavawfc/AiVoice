package com.imooc.lib_base

import android.app.Application
import android.content.Intent
import com.imooc.lib_base.helper.ARouterHelper
import com.imooc.lib_base.service.InitService
import com.imooc.lib_base.utils.SpUtils

/**
 * 基类App
 */
open class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        ARouterHelper.initHelper(this)
        startService(Intent(this,InitService::class.java))
    }
}