package com.imooc.lib_base.service

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.util.Log
import com.imooc.lib_base.helper.NotificationHelper
import com.imooc.lib_base.utils.SpUtils
import com.imooc.lib_voice.manager.VoiceManager

/**
 * FileName: InitService
  * Profile: 初始化服务
 */
class InitService : IntentService("InitService") {

    private val TAG = "InitService"

    override fun onHandleIntent(intent: Intent?) {
        Log.d(TAG,"onHandleIntent ")
        SpUtils.initUtils(this)
        NotificationHelper.initHelper(this)
        VoiceManager.initManager(this)
    }







}