package com.imooc.lib_base.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imooc.lib_base.R

abstract  class BaseFragment : Fragment() {



    //获取布局ID
    abstract fun getLayoutId():Int
    //获取标题
    abstract fun getTitleText():String

    //初始化操作
    abstract  fun initView()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(getLayoutId(), null, false)
        return inflater.inflate(getLayoutId(), null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        initView()

    }


}