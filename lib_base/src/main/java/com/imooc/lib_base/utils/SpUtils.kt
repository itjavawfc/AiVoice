package com.imooc.lib_base.utils

import android.content.Context
import android.content.SharedPreferences

/***
 *
 */
object SpUtils {
    private   val TAG = "SpUtils"
    private   val SP_NAME = "config"

    //对象
    private lateinit var sp:SharedPreferences
    private lateinit var spEditor:SharedPreferences.Editor

    //初始化数据
    fun initUtils(mContext : Context){
        sp=mContext.getSharedPreferences(SP_NAME,Context.MODE_PRIVATE)
        spEditor=sp.edit()
        spEditor.apply()
    }

    fun putString(key:String,value:String){
        spEditor.putString(key,value)
        spEditor.commit()
    }

    fun getString(key:String): String? {
        return sp.getString(key,"")
    }


    fun putInt(key:String,value:Int){
        spEditor.putInt(key,value)
        spEditor.commit()
    }

    fun getInt(key:String,default:Int): Int? {
        return sp.getInt(key,default)
    }

}