package com.imooc.lib_base.utils

import android.util.Log
import com.alibaba.android.arouter.BuildConfig

/***
 * Log 日志工具类
 */
object L {

    private   val TAG = "AiVoiceApp"

    fun  i(text:String){
        text?.let {
            Log.i(TAG,it)
        }
    }
    fun  d(text:String){
        text?.let {
            Log.d(TAG,it)
        }
    }
    fun  e(text:String){
        text?.let {
            Log.e(TAG,it)
        }
    }

}