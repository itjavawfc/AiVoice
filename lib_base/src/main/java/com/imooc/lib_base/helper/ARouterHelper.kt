package com.imooc.lib_base.helper

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.alibaba.android.arouter.launcher.ARouter
import java.util.ListResourceBundle

/***
 * 路由的帮助类
 */
object ARouterHelper {


    const val PATH_APP_MANAGER = "/app_manager/app_manager_activity"
    const val PATH_CONSTELLATION = "/constellation/constellation_activity"
    const val PATH_DEVELOPER = "/developer/developer_activity"
    const val PATH_JOKE = "/joke/joke_activity"
    const val PATH_MAP = "/map/map_activity"
    const val PATH_MAP_NAVI = "/map/navi_activity"
    const val PATH_SETTING = "/setting/setting_activity"
    const val PATH_VOICE_SETTING = "/voice/voice_setting_activity"
    const val PATH_WEATHER = "/weather/weather_activity"
    const val PATH_TEST_RY = "/app/test_ryView"


    //初始化
    fun  initHelper(application:Application){
       // if (isDebug()) {           // These two lines must be written before init, otherwise these configurations will be invalid in the init process
            ARouter.openLog();     // Print log
            ARouter.openDebug();   // Turn on debugging mode (If you are running in InstantRun mode, you must turn on debug mode! Online version needs to be closed, otherwise there is a security risk)
      //  }
        ARouter.init(application); //
    }

    fun startActivity(path:String){
        ARouter.getInstance().build(path).navigation()
    }

    fun startActivity(activity: Activity, path:String,requestCode:Int){
        ARouter.getInstance().build(path).navigation(activity,requestCode)
    }

    fun startActivity(path:String,key:String,value:String){
        ARouter.getInstance().build(path).withString(key,value).navigation()
    }

    fun startActivity(path:String,key:String,value:Int){
        ARouter.getInstance().build(path).withInt(key,value).navigation()
    }

    fun startActivity(path:String,key:String,value:Boolean){
        ARouter.getInstance().build(path).withBoolean(key,value).navigation()
    }

    fun startActivity(path:String,key:String,bundle: Bundle){
        ARouter.getInstance().build(path).withBundle(key,bundle).navigation()
    }

    fun startActivity(path:String,key:String,any: Any){
        ARouter.getInstance().build(path).withObject(key,any).navigation()
    }



}