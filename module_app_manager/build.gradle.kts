plugins {
   // id("com.android.application")
    //id("com.android.library")
    if (ModuleConfig.isApp) {
        id("com.android.application")
    } else {
        id("com.android.library")
    }


    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
}

android {
    namespace = "com.imooc.appmanager"
    compileSdk = 34

    defaultConfig {
      //   applicationId = "com.imooc.appmanager"
        minSdk = 24
        targetSdk = 33
      //   versionCode = 1
      //   versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        kapt {
            arguments {
                // arguments = [AROUTER_MODULE_NAME: project.getName()]
                // arguments = mapOf("AROUTER_MODULE_NAME" to project.name)
                arg("AROUTER_MODULE_NAME" , project.name)
            }
        }
    }
//签名类型 配置
    signingConfigs{
        register("release"){
            //别名
            keyAlias = "aivoice"
            //别名密码
            keyPassword = "123456"
            //路径
            storeFile=file("/src/main/jks/aivoice.jks")
            //签名密码
            storePassword = "123456"

        }
    }



    //编译类型
    buildTypes {
        getByName("debug"){

        }
        /*  release {
              isMinifyEnabled = false  //是否开启混淆
              proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
          }*/
        getByName("release"){
            isMinifyEnabled = false  //是否开启混淆
            // 自动签名打包
            signingConfig=signingConfigs.getByName("release")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    // 依赖操作
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    //输出类型
  /*  android.applicationVariants.all {
        //编译类型
        val buildType=this.buildType
        outputs.all {
            if(this is com.android.build.gradle.internal.api.ApkVariantOutputImpl){
                this.outputFileName="AI_V${defaultConfig.versionName}_${buildType.name}.apk"
            }
        }


    }*/

    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.activity:activity-compose:1.8.1")
    implementation(platform("androidx.compose:compose-bom:2023.10.01"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3:1.1.2")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.10.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")

    debugImplementation("androidx.compose.ui:ui-tooling")
    api(project(":lib_base"))
    kapt("com.alibaba:arouter-compiler:1.5.2")
}