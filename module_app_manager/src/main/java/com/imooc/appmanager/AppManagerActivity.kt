package com.imooc.appmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.alibaba.android.arouter.facade.annotation.Route
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.helper.ARouterHelper
import com.imooc.lib_base.utils.L


@Route(path = ARouterHelper.PATH_APP_MANAGER)
class AppManagerActivity : BaseActivity() {
    private   val TAG = "AppManagerActivity"
    override fun getLayoutId(): Int {
        return  R.layout.activity_app_manager
    }

    override fun getTitleText(): String {
        return getString(R.string.app_name)
    }

    override fun initView() {
       L.d(" initView")
    }

    override fun isShowBack(): Boolean {
        return  false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_app_manager)
    }
}