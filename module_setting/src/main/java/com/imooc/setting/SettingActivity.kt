package com.imooc.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.utils.L

class SettingActivity : BaseActivity() {
    private   val TAG = "SettingActivity"
    override fun getLayoutId(): Int {
       return  R.layout.activity_setting
    }

    override fun getTitleText(): String {
        return getString(R.string.app_name)
    }

    override fun initView() {
       L.d("initView ")
    }

    override fun isShowBack(): Boolean {
        return true
    }


}