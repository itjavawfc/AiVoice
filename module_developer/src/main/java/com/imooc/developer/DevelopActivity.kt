package com.imooc.developer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.arouter.facade.annotation.Route
import com.imooc.developer.data.DeveloperListData
import com.imooc.lib_base.adapter.CommonAdapter
import com.imooc.lib_base.adapter.CommonViewHolder
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.helper.ARouterHelper
import com.imooc.lib_base.utils.L
import com.imooc.lib_voice.manager.VoiceManager
import com.imooc.lib_voice.tts.VoiceTTS
import com.imooc.lib_voice.tts.VoiceTTS.OnTTSResultListener

@Route(path = ARouterHelper.PATH_DEVELOPER)
class DevelopActivity : BaseActivity() {

    private   val TAG = "DevelopActivity"

    //标题
    private val mTypeTitle=0
    //内容
    private val mTypeContent=1

    private var mList=ArrayList<DeveloperListData>()
    private lateinit var  rvDeveloperView:RecyclerView


    private val ttsStr="深圳，简称“深”，别称鹏城，势东南高";

    override fun getLayoutId(): Int {
        return R.layout.activity_develop
    }

    override fun getTitleText(): String {
       return getString(R.string.app_name)
    }

    override fun initView() {
       L.d("initView ")
        initData();
        initListView();
    }

    private fun initListView() {
        Log.d(TAG,"初始化列表")
        rvDeveloperView=findViewById(R.id.rvDeveloperView)
        //布局管理器
        rvDeveloperView.layoutManager=LinearLayoutManager(this)
        //分割线
        rvDeveloperView.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        //适配器
        rvDeveloperView.adapter=CommonAdapter(mList,object :CommonAdapter.OnMoreBindDataListener<DeveloperListData>{
            override fun onBindViewHolder(mode: DeveloperListData, viewHolder: CommonViewHolder, type: Int, position: Int) {
                 when(mode.type){
                     mTypeTitle->{
                        viewHolder.setText(R.id.mTvDeveloperTitle,mode.text)
                     }
                     mTypeContent->{
                         viewHolder.setText(R.id.mTvDeveloperContent,"${position}.${mode.text}")
                         viewHolder.itemView.setOnClickListener{
                             itemClickFun(position)
                         }
                     }
                 }
            }

            override fun getItemType(position: Int): Int {
                return mList[position].type
            }

            override fun getLayoutId(type: Int): Int {
                return if(type==mTypeTitle){
                    R.layout.layout_developer_title
                }else{
                    R.layout.layout_developer_content
                }
            }
        });

     }

    private fun itemClickFun(position: Int) {
        Log.d(TAG," posistion:$position")
        when(position){
            0->ARouterHelper.startActivity(ARouterHelper.PATH_APP_MANAGER)
            1->ARouterHelper.startActivity(ARouterHelper.PATH_CONSTELLATION)
            2->ARouterHelper.startActivity(ARouterHelper.PATH_JOKE)
            3->ARouterHelper.startActivity(ARouterHelper.PATH_MAP)
            4->ARouterHelper.startActivity(ARouterHelper.PATH_SETTING)
            5->ARouterHelper.startActivity(ARouterHelper.PATH_VOICE_SETTING)
            6->ARouterHelper.startActivity(ARouterHelper.PATH_WEATHER)

            20->VoiceManager.ttsStart(ttsStr)
            21->VoiceManager.ttsPause()
            22->VoiceManager.ttsResume()
            23->VoiceManager.ttsStop()
            24->VoiceManager.ttsRelease()
        }


    }

    private fun initData() {
        Log.d(TAG,"初始化数据")
        val dataArray=resources.getStringArray(com.imooc.lib_base.R.array.DeveloperListArray)
        dataArray.forEach {
            //说明是标题
            if(it.contains("[")){
                addItemData(mTypeTitle,it.replace("[","").replace("]",""))
            }else{
                addItemData(mTypeContent,it)

            }
        }


     }

    override fun isShowBack(): Boolean {
       return  true;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     }

    //添加数据
    private fun addItemData(type:Int,text:String){
        mList.add(DeveloperListData(type,text));
    }



}