import com.android.build.gradle.internal.api.ApkVariantOutputImpl

//引用插件
plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
   // kotlin("‘kotlin-android-extensions")
  //  id("kotlin-android-extensions")
 }

// Android 属性
android {
    namespace = "com.imooc.aivoice"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.imooc.aivoice"
        minSdk = 24
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }


      /*  javaCompileOptions {
            annotationProcessorOptions {
               // arguments = [AROUTER_MODULE_NAME: project.getName()]
               // arguments = mapOf("AROUTER_MODULE_NAME" to project.name)
                 arguments(mapOf("AROUTER_MODULE_NAME" to project.name))
            }
        }*/
        kapt {
            arguments {
               // arguments = [AROUTER_MODULE_NAME: project.getName()]
               // arguments = mapOf("AROUTER_MODULE_NAME" to project.name)
                 arg("AROUTER_MODULE_NAME" , project.name)
            }
        }

    }

    viewBinding.enable=true
//签名类型 配置
    signingConfigs{
        register("release"){
            //别名
            keyAlias = "aivoice"
            //别名密码
            keyPassword = "123456"
            //路径
            storeFile=file("/src/main/jks/aivoice.jks")
            //签名密码
            storePassword = "123456"

        }
    }



    //编译类型
    buildTypes {
        getByName("debug"){

        }
      /*  release {
            isMinifyEnabled = false  //是否开启混淆
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }*/
        getByName("release"){
            isMinifyEnabled = false  //是否开启混淆
            // 自动签名打包
            signingConfig=signingConfigs.getByName("release")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    // 依赖操作
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    //输出类型
    android.applicationVariants.all {
        //编译类型
        val buildType=this.buildType
        outputs.all {
            if(this is ApkVariantOutputImpl){
                this.outputFileName="AI_V${defaultConfig.versionName}_${buildType.name}.apk"
            }
        }


    }

    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

// 依赖
dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    //noinspection GradleCompatible
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.activity:activity-compose:1.8.1")
    implementation(platform("androidx.compose:compose-bom:2023.10.01"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3:1.1.2")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.10.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation(project(mapOf("path" to ":module_app_manager")))
    implementation(project(mapOf("path" to ":module_constellation")))
    implementation(project(mapOf("path" to ":module_setting")))
    implementation(project(mapOf("path" to ":module_voice_setting")))
    implementation(project(mapOf("path" to ":module_joke")))
    implementation(project(mapOf("path" to ":module_developer")))
    implementation(project(mapOf("path" to ":module_map")))
    implementation(project(mapOf("path" to ":module_weather")))
    debugImplementation("androidx.compose.ui:ui-tooling")
    api(project(":lib_base"))

  //  annotationProcessor("com.alibaba:arouter-compiler:1.5.2")
    //运行时注解
    kapt("com.alibaba:arouter-compiler:1.5.2")
    //annotationProcessor("com.alibaba:arouter-compiler:1.5.2")   //运行时注解
/*     implementation("com.alibaba:arouter-api:1.5.2")
    annotationProcessor("com.alibaba:arouter-compiler:1.5.2")*/

}