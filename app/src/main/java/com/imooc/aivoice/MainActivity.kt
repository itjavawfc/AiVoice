package com.imooc.aivoice
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.imooc.aivoice.service.VoiceService
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.event.EventManager
import com.imooc.lib_base.event.MessageEvent
import com.imooc.lib_base.helper.ARouterHelper
import com.imooc.lib_base.utils.L
import com.imooc.lib_base.utils.SpUtils
 import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.Random

class MainActivity : BaseActivity() {
    private val TAG = "MainActivity"
    override fun getLayoutId(): Int {
         return R.layout.activity_main;
    }

    override fun getTitleText(): String {
        return getString(R.string.app_name)
    }

    override fun initView() {
       L.d(" initView")

        findViewById<Button>(R.id.btn1).setOnClickListener {
            EventManager.post(10001, "Hello")
        }
        findViewById<Button>(R.id.btn2).setOnClickListener {
            EventManager.post(10002, 1234567890)
        }
        findViewById<Button>(R.id.btn3).setOnClickListener {
            EventManager.post(10003, true)
        }

        findViewById<Button>(R.id.goforward).setOnClickListener(View.OnClickListener {
            ARouterHelper.startActivity(ARouterHelper.PATH_APP_MANAGER)
        })


        findViewById<Button>(R.id.write_sp).setOnClickListener(View.OnClickListener {
                var randStr=randomString(5)
            SpUtils.putString("KEY",randStr)


        })


        findViewById<Button>(R.id.get_sp).setOnClickListener(View.OnClickListener {
            var getRandStr=SpUtils.getString("KEY")
            if (getRandStr != null) {
                L.d(getRandStr)
            }
        })
        findViewById<Button>(R.id.to_ry).setOnClickListener(View.OnClickListener {
            ARouterHelper.startActivity(ARouterHelper.PATH_TEST_RY)
        })
        findViewById<Button>(R.id.to_developer).setOnClickListener(View.OnClickListener {
            ARouterHelper.startActivity(ARouterHelper.PATH_DEVELOPER)
        })

        Log.d(TAG," 启动语音服务 VoiceService")
        startService(Intent(this,VoiceService::class.java))
    }

    override fun isShowBack(): Boolean {
        return true;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_main)
        EventManager.register(this)



    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent?) {
        when(event!!.type){
            10001->L.d(event.stringValue)
            10002->L.d( event.intValue.toString())
            10003->L.d( event.booleanValue.toString())
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        EventManager.unRegister(this)
    }


    fun randomString(length: Int): String {
        val chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        val random = Random()
        val sb = StringBuilder(length)
        for (i in 0 until length) {
            val index = random.nextInt(chars.length)
            sb.append(chars[index])
        }
        return sb.toString()
    }

}