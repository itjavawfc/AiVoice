package com.imooc.aivoice.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.arouter.facade.annotation.Route
import com.imooc.aivoice.R
import com.imooc.lib_base.adapter.CommonAdapter
import com.imooc.lib_base.adapter.CommonViewHolder
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.helper.ARouterHelper
import com.imooc.lib_base.utils.L


@Route(path = ARouterHelper.PATH_TEST_RY)
class RyViewTestActivity : BaseActivity() {
    private   val TAG = "RyViewTestActivity"

    private   var mList:ArrayList<String> = ArrayList()
    lateinit var mRecycleView:RecyclerView

    override fun getLayoutId(): Int {
        return R.layout.activity_ry_view_test
    }

    override fun getTitleText(): String {
         return "RecycleView 测试验证"
    }

    override fun initView() {
        L.d("initView")
        mRecycleView=findViewById(R.id.mRecycleView)
        //模拟数据
        for(i in 1..50){
           mList.add("第${i}个元素")
        }
        mRecycleView.layoutManager=LinearLayoutManager(this)
        mRecycleView.adapter=CommonAdapter<String>(mList,object :CommonAdapter.OnBindDataListener<String>{
            override fun onBindViewHolder(mode: String, viewHolder: CommonViewHolder, type: Int, position: Int) {
                viewHolder.setText(R.id.textView,mode)
            }

            override fun getLayoutId(type: Int): Int {
                return R.layout.item_ry_view
            }

        });

    }

    override fun isShowBack(): Boolean {
        return  true
    }

}