package com.imooc.lib_voice.tts

import android.content.Context
import android.util.Log
import com.baidu.tts.client.SpeechError
import com.baidu.tts.client.SpeechSynthesizer
import com.baidu.tts.client.SpeechSynthesizerListener
import com.baidu.tts.client.TtsMode
import com.imooc.lib_voice.manager.VoiceManager

/***
 * AppID:45855813
 * API Key:DG2rakGrtqOMfUIlTGvXeV4O
 * Secret Key:vmFT4fo6EXqHsl4cHWI5KKyqVakru5fb
 *
 *  * Profile: 百度AI语音 - TTS 封装
 *  *
 *  * 1.实现其他参数
 *  * 2.实现监听播放结束
 *
 */
object   VoiceTTS : SpeechSynthesizerListener {

    /**
     * 假设：我们有一个需求
     * 就是当TTS播放结束的时候执行一段操作
     */

    private var TAG = VoiceTTS::class.java.simpleName

    //TTS对象
    private lateinit var mSpeechSynthesizer: SpeechSynthesizer

    //接口对象
    private var mOnTTSResultListener: OnTTSResultListener? = null

    //初始化TTS
    fun initTTS(mContext: Context) {
        //初始化对象
        mSpeechSynthesizer = SpeechSynthesizer.getInstance()
        //设置上下文
        mSpeechSynthesizer.setContext(mContext)
        //设置Key
        mSpeechSynthesizer.setAppId(VoiceManager.VOICE_APP_ID)
        mSpeechSynthesizer.setApiKey(VoiceManager.VOICE_APP_KEY, VoiceManager.VOICE_APP_SECRET)
        //设置监听
        mSpeechSynthesizer.setSpeechSynthesizerListener(this)

        //初始化
        mSpeechSynthesizer.initTts(TtsMode.ONLINE)
        Log.i(TAG, "TTS init")
    }

    //设置发音人
    fun setPeople(people: String) {
        Log.d(TAG," 设置发音人:"+people)
        mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, people)
    }

    //设置语速
    fun setVoiceSpeed(speed: String) {
        Log.d(TAG," 设置语速:"+speed)
        mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, speed)
    }

    //设置音量
    fun setVoiceVolume(volume: String) {
        Log.d(TAG," 设置音量:"+volume)
        mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, volume)
    }

    override fun onSynthesizeStart(p0: String?) {
        Log.d(TAG," 合成开始:"+p0)

        Log.i(TAG, "合成开始")
    }

    override fun onSpeechFinish(p0: String?) {
        Log.i(TAG, "播放结束:"+p0)
        mOnTTSResultListener?.ttsEnd()
    }

    override fun onSpeechProgressChanged(p0: String?, p1: Int) {
        Log.i(TAG, "onSpeechProgressChanged:p0:"+p0+"    p1:"+p1)

    }

    override fun onSynthesizeFinish(p0: String?) {
         Log.i(TAG, "合成结束 onSynthesizeFinish p0:"+p0 )

    }

    override fun onSpeechStart(p0: String?) {
         Log.i(TAG, "开始播放 onSpeechStart p0:"+p0 )

    }

    override fun onSynthesizeDataArrived(p0: String?, p1: ByteArray?, p2: Int, p3: Int) {
        Log.i(TAG, "onSynthesizeDataArrived " )

    }

    override fun onError(string: String?, error: SpeechError?) {
         Log.i(TAG, "TTS 错误  onError:$string:$error" )

    }

    //播放并且有回调
    fun start(text: String, mOnTTSResultListener: OnTTSResultListener?) {
        Log.i(TAG, "播放并且有回调  start   text:"+text )

        this.mOnTTSResultListener = mOnTTSResultListener
        mSpeechSynthesizer.speak(text)
    }

    //暂停
    fun pause() {
        Log.i(TAG, "暂停   pause:" )
        mSpeechSynthesizer.pause()
    }

    //继续播放
    fun resume() {
        Log.i(TAG, "继续播放   resume:" )
        mSpeechSynthesizer.resume()
    }

    //停止播放
    fun stop() {
        Log.i(TAG, "停止播放   stop:" )

        mSpeechSynthesizer.stop()
    }

    //释放
    fun release() {
        Log.i(TAG, "释放   release:" )
        mSpeechSynthesizer.release()
    }

    //接口
    interface OnTTSResultListener {
        //播放结束
        fun ttsEnd()
    }
}
