package com.imooc.joke

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.imooc.lib_base.base.BaseActivity
import com.imooc.lib_base.utils.L

class JokeActivity : BaseActivity() {
    private   val TAG = "JokeActivity"
    override fun getLayoutId(): Int {
       return R.layout.activity_joke
    }

    override fun getTitleText(): String {
        return getString(R.string.app_name)
    }

    override fun initView() {
       L.d(" initView")
    }

    override fun isShowBack(): Boolean {
        return  true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     }
}