apply {
    plugin("kotlin")
}


buildscript {
    repositories {
        gradlePluginPortal()
    }
    dependencies {
        classpath(kotlin("gradle-plugin", "1.8.10"))
    }
}
dependencies {
    implementation(gradleKotlinDsl())
    implementation(kotlin("stdlib", "1.8.10"))
}
repositories {
    gradlePluginPortal()
}